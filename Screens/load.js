﻿

var loadState = {

    preload: function () {
        game.load.image('loading', 'Images/loading.jpg');
        game.load.spritesheet('start', 'Pictures/start_.png', 120, 40);
    },
    

    create: function () {
        var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'loading');
        Background.anchor.setTo(0.5, 0.5);
        var startButton = game.add.button(game.world.centerX -55, game.world.centerY +80, 'start', this.start, this, 1, 0, 2);
        var wKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        wKey.onDown.addOnce(this.start, this);

    },
    start: function () {
        game.state.start('mainmenu');
        
    }

};



//var gameWidth = 780;
//var gameHeight = 250;

//var loadState = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'gameDiv', {
//    preload: preload, create: create
//});


//function preload() {

          

//           game.load.image('loading', 'Images/loading.jpg');
//}

//function create() {

//    var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'loading');
//            //Background.anchor.setTo(0.5, 0.5);

//            var wKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
//            wKey.onDown.addOnce(this.start, this);
//}

//function start() {

//    game.state.start('ScreenManager');
//}