﻿var bg;
var bg2;
var bg4;
var bg5;

var timer;

var backState = {

    preload:function(){
        game.load.image('background', 'Pictures/back_select.jpg');
        game.load.spritesheet('bg1S', 'Pictures/bg1S.png',193,71);
        game.load.spritesheet('bg2S', 'Pictures/bg2S.png', 193, 71);
        game.load.spritesheet('bg4S', 'Pictures/bg4S.png', 193, 71);
        game.load.spritesheet('bg5S', 'Pictures/bg5S.png', 193, 71);

    },
    bgN: false,
    bg2N: false,
    bg4N: false,
    bg5N: false,

    create: function () {

        

        var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'background');
        Background.anchor.setTo(0.5, 0.5);
        
         bg = game.add.button(250, 30, 'bg1S', this.actionOnClick, this, 0, 0, 0);
         bg2 = game.add.button(bg.position.x, bg.position.y + 100, 'bg2S', this.actionOnClick, this, 0, 0, 0);
         bg4 = game.add.button(bg2.position.x + 130, 130, 'bg4S', this.actionOnClick, this, 0, 0, 0);
         bg5 = game.add.button(bg.position.x + 130, 30, 'bg5S', this.actionOnClick, this, 0, 0, 0);
        

         bg.onInputOver.add(this.over, this);
         bg2.onInputOver.add(this.over, this);
         bg4.onInputOver.add(this.over, this);
         bg5.onInputOver.add(this.over, this);

         timer = game.time.create(false);
         

    },
    actionOnClick: function (button) {


        switch (button) {
            case bg:
                this.bgN = true;
                break;
            case bg2:
                this.bg2N = true;
                break;
            case bg4:
                this.bg4N = true;
                break;
            case bg5:
                this.bg5N = true;
                break;
            default:
                break;
        }

        mainState.selected.play();
        timer.loop(2000, this.start, this);
        timer.start();

        

    },
    over : function(){
        mainState.clickover.play();
    },
    start: function () {
        this.game.world.removeAll();
        
        mainState.charselect.stop();
        game.state.start('ScreenManager');
    },
};