﻿var gameWidth = 780;
var gameHeight = 250;


var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'gameDiv');


game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('mainmenu', mainState);
game.state.add('backgroundmenu', backState);
game.state.add('ScreenManager', gameState);
game.state.start('boot');

