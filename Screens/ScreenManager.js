﻿var poisonBool = false;

var counter = 0;
var counterPoison = 0;

var buttonReplay;

var buttonHPotion;
var buttonMPotion;
var buttonTPotion;

var manaTextTimer;
var turnTextTimer;


var turn = 0;
var previousTurn = turn;

var turnPotionCountW1 = 1;
var turnPotionCountW2 = 1;
var turnPotionCountW3 = 1;
var turnPotionCountW4 = 1;

var buttonPierceTurn = 2;
var buttonDrainTurn = 1;
var buttonPoisonTurn = 1;

var poisonTurn = 3;

var buttonDecreaseTurn = 1;
var buttonStaffTurn = 2;
var buttonDarkspellTurn = 1;

var buttonHeavysoulTurn = 2;
var buttonEvasionTurn = 1;
var buttonBurnTurn = 1;

var buttonElectricTurn = 2;
var buttonRandomTurn = 2;
var buttonTurnerTurn = 1;


var hp = 100;
var mana = 100;
var hp2 = 100;
var mana2 = 100;
var hp3 = 100;
var mana3 = 100;
var hp4 = 100;
var mana4 = 100;


var manaPierce = 7;
var manaDrain = 5;
var manaPoison = 5;

var damagePierce = 15;
var damageDrain = 10;
var damagePoison = 5;

var manaDecrease = 10;
var manaStaff = 7;
var manaDarkSpell = 8;

var damageDecrease = 5;
var damageStaff = 15;
var damageDarkSpell = 15;

var manaHeavysoul = 12;
var manaBurn = 15;
var manaEvasion = 5;

var damageHeavysoul = 22;
var damageEvasion ;
var damageBurn = 15;

var manaElectric = 10;
var manaRandom = 20;
var manaTurner = 25;

var damageElectric = 15;
var damageRandom ;


var manaSound;
var healSound;
var replenish;

var drainSound;
var poisonSound;
var pierceSound;

var darkspellSound;
var staffspellSound;
var decreaseSound;

var heavysoulSound;
var evasionSound;
var burnSound;

var electricSound;
var randomSound;
var turnerSound;

var mainSound;
var mainSound2;
var mainSound3;
var mainSound4;

var youWinSound;

var textCenter;
var text2;
var text3;
var text4;
var text5;

var textPotionCheck;

var buttonPierce;
var buttonDrain;
var buttonPoison;

var buttonDecrease;
var buttonStaff;
var buttonDarkspell;

var myHealthBar;
var myManaBar;

var myHealthBar2;
var myManaBar2;

var myHealthBar3;
var myManaBar3;

var myHealthBar4;
var myManaBar4;


var noMana = false;
var noHealth;

var Characters = [];

var W2bool = true;
var W3bool = true;

var showLogs = true;
if (showLogs) {
    console.log("ScreenManager");
}

//var gameWidth = 780;
//var gameHeight = 250;
//var backgroundColor = "#000000";

//var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'gameDiv', {
//    preload: preload, create: create, update: update
//});

var gameState = {
    
    preload : function() {
    if (showLogs) {
        console.log("ScreenManager ­ preload");
}


game.load.image('background', 'Pictures/bg_.png');
game.load.image('background2', 'Pictures/bg2.png');
game.load.image('background3', 'Pictures/bg4.png');
game.load.image('background4', 'Pictures/bg5.png');


game.load.atlas('wizard1', 'Sprites/wizard.png', 'Sprites/wizard_.json',
Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

game.load.atlas('wizard2', 'Sprites/wizard2_.png', 'Sprites/wizard2__.json',
Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

game.load.atlas('wizard3', 'Sprites/wizard3_.png', 'Sprites/wizard3.json',
Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

game.load.atlas('wizard4', 'Sprites/wizard4.png', 'Sprites/wizard4.json',
Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

game.load.audio('drain', 'SoundEffects/drain.mp3');
game.load.audio('poison', 'SoundEffects/poison.mp3');
game.load.audio('pierce', 'SoundEffects/pierce.mp3');

game.load.audio('darkspell', 'SoundEffects/darkspell.mp3');
game.load.audio('staffspell', 'SoundEffects/staffspell.mp3');
game.load.audio('decrease', 'SoundEffects/decrease.mp3');

game.load.audio('heavysoul', 'SoundEffects/heavysoul.mp3');
game.load.audio('burn', 'SoundEffects/burn.mp3');
game.load.audio('evasion', 'SoundEffects/evasion.mp3');

game.load.audio('electric', 'SoundEffects/electric.mp3');
game.load.audio('random', 'SoundEffects/random.mp3');
game.load.audio('turner', 'SoundEffects/turner.mp3');

game.load.audio('mana', 'SoundEffects/mana.mp3');
game.load.audio('heal', 'SoundEffects/heal.mp3');
game.load.audio('replenish', 'SoundEffects/replenish.mp3');
game.load.audio('main', 'SoundEffects/main.mp3');
game.load.audio('main2', 'SoundEffects/main2.mp3');
game.load.audio('main3', 'SoundEffects/main3.mp3');
game.load.audio('main4', 'SoundEffects/main4.mp3');
game.load.audio('youWin', 'SoundEffects/YouWin.mp3');

game.load.spritesheet('buttonReplay', 'ButtonImages/play-again.png',193,71);

game.load.spritesheet('buttonHPotion', 'ButtonImages/healthpotion.png', 193, 71);
game.load.spritesheet('buttonMPotion', 'ButtonImages/manapotion.png', 193, 71);
game.load.spritesheet('buttonTPotion', 'ButtonImages/turnpotion.png', 193, 71);

game.load.spritesheet('buttonPierce', 'ButtonImages/pierce.png', 193, 71);
game.load.spritesheet('buttonDrain', 'ButtonImages/drain.png', 193, 71);
game.load.spritesheet('buttonPoison', 'ButtonImages/poison.png', 193, 71);

game.load.spritesheet('buttonDarkspell', 'ButtonImages/darkspell2.png', 193, 71);
game.load.spritesheet('buttonDecrease', 'ButtonImages/decrease.png', 193, 71);
game.load.spritesheet('buttonStaff', 'ButtonImages/staff.png', 193, 71);

game.load.spritesheet('buttonHeavysoul', 'ButtonImages/heavysoul.png', 193, 71);
game.load.spritesheet('buttonEvasion', 'ButtonImages/evasion.png', 193, 71);
game.load.spritesheet('buttonBurn', 'ButtonImages/burn.png', 193, 71);

game.load.spritesheet('buttonElectric', 'ButtonImages/electric.png', 193, 71);
game.load.spritesheet('buttonRandom', 'ButtonImages/random.png', 193, 71);
game.load.spritesheet('buttonTurner', 'ButtonImages/turner.png', 193, 71);
    },
    
    damageRandom:0,
    create: function () {

       

        

        mainSound = game.add.audio('main');

        mainSound2 = game.add.audio('main2');

        mainSound3 = game.add.audio('main3');

        mainSound4 = game.add.audio('main4');

        youWinSound = game.add.audio('youWin');


    if (showLogs)
        console.log("ScreenManager ­ create");


    if (backState.bgN == true) {
        var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'background');
        Background.anchor.setTo(0.5, 0.5);
        mainSound2.play();
        

    }
    if (backState.bg2N == true) {
        var Background2 = game.add.sprite(game.world.centerX, game.world.centerY, 'background2');
        Background2.anchor.setTo(0.5, 0.5);
        mainSound4.play();
    }

    if (backState.bg4N == true) {
        var Background3 = game.add.sprite(game.world.centerX, game.world.centerY, 'background3');
        Background3.anchor.setTo(0.5, 0.5);
        mainSound3.play();
    }
        if (backState.bg5N == true) {
        var Background4 = game.add.sprite(game.world.centerX, game.world.centerY, 'background4');
        Background4.anchor.setTo(0.5, 0.5);
        mainSound.play();
    }


        buttonPierce = game.add.button();
        buttonDrain = game.add.button();
        buttonPoison = game.add.button();

        buttonDarkspell = game.add.button();
        buttonDecrease = game.add.button();
        buttonStaff = game.add.button();


        buttonHeavysoul = game.add.button();
        buttonEvasion = game.add.button();
        buttonBurn = game.add.button();

        buttonElectric = game.add.button();
        buttonRandom = game.add.button();
        buttonTurner = game.add.button();


if (mainState.selectionboolW1 == true) {
    var player1 = new Character("Wizard1");
    player1.init();
    Characters[0] = player1;
    //Characters.push(player1);

    var barConfigHealth = { x: mainState.barHW1, y: 22 };
    var barConfigMana = { x: mainState.barMW1, y: 42 };

    myHealthBar = new HealthBar(game, barConfigHealth);
    myManaBar = new ManaBar(game, barConfigMana);



    buttonPierce = game.add.button(mainState.buttonPozW1, 53, 'buttonPierce', this.actionOnClick, this, 0, 0, 0);
    buttonDrain = game.add.button(buttonPierce.position.x - 40, 53, 'buttonDrain', this.actionOnClick, this, 0, 0, 0);
    buttonPoison = game.add.button(buttonDrain.position.x - 40, 53, 'buttonPoison', this.actionOnClick, this, 0, 0, 0);

    buttonPierce.onInputOver.add(this.over, this);
    buttonPierce.onInputOut.add(this.out, this);
    buttonDrain.onInputOver.add(this.over, this);
    buttonDrain.onInputOut.add(this.out, this);
    buttonPoison.onInputOver.add(this.over, this);
    buttonPoison.onInputOut.add(this.out, this);
}

if (mainState.selectionboolW2 == true) {

    var player2 = new Character("Wizard2");
    player2.init2();
    //Characters.push(player2);

    Characters[1] = player2;

    console.log(mainState.selectionboolW3);

    var barConfigHealth2 = { x: mainState.barHW2, y: 22 };
    var barConfigMana2 = { x: mainState.barMW2, y: 42 };

    myHealthBar2 = new HealthBar(game, barConfigHealth2);
    myManaBar2 = new ManaBar(game, barConfigMana2);

    buttonDarkspell = game.add.button(mainState.buttonPozW2, 53, 'buttonDarkspell', this.actionOnClick, this, 0, 0, 0);
    buttonDecrease = game.add.button(buttonDarkspell.position.x + 40, 53, 'buttonDecrease', this.actionOnClick, this, 0, 0, 0);
    buttonStaff = game.add.button(buttonDecrease.position.x + 40, 53, 'buttonStaff', this.actionOnClick, this, 0, 0, 0);

    buttonDarkspell.onInputOver.add(this.over, this);
    buttonDarkspell.onInputOut.add(this.out, this);
    buttonDecrease.onInputOver.add(this.over, this);
    buttonDecrease.onInputOut.add(this.out, this);
    buttonStaff.onInputOver.add(this.over, this);
    buttonStaff.onInputOut.add(this.out, this);
}

if (mainState.selectionboolW3 == true) {

    var player3 = new Character("Wizard3");
    player3.init3();

    Characters[2] = player3;
        var barConfigHealth3 = { x: mainState.barHW3, y: 22 };
        var barConfigMana3 = { x: mainState.barMW3, y: 42 };

        myHealthBar3 = new HealthBar(game, barConfigHealth3);
        myManaBar3 = new ManaBar(game, barConfigMana3);


        buttonHeavysoul = game.add.button(mainState.buttonPozW3, 53, 'buttonHeavysoul', this.actionOnClick, this, 0, 0, 0);
        buttonEvasion = game.add.button(buttonHeavysoul.position.x + 40, 53, 'buttonEvasion', this.actionOnClick, this, 0, 0, 0);
        buttonBurn = game.add.button(buttonEvasion.position.x + 40, 53, 'buttonBurn', this.actionOnClick, this, 0, 0, 0);

        buttonHeavysoul.onInputOver.add(this.over, this);
        buttonHeavysoul.onInputOut.add(this.out, this);
        buttonEvasion.onInputOver.add(this.over, this);
        buttonEvasion.onInputOut.add(this.out, this);
        buttonBurn.onInputOver.add(this.over, this);
        buttonBurn.onInputOut.add(this.out, this);

}

if (mainState.selectionboolW4 == true) {

    var player4 = new Character("Wizard4");
    player4.init4();
    Characters[3] = player4;

    var barConfigHealth4 = { x: mainState.barHW4, y: 22 };
    var barConfigMana4 = { x: mainState.barMW4, y: 42 };

    myHealthBar4 = new HealthBar(game, barConfigHealth4);
    myManaBar4 = new ManaBar(game, barConfigMana4);


    buttonElectric = game.add.button(mainState.buttonPozW4, 53, 'buttonElectric', this.actionOnClick, this, 0, 0, 0);
    buttonRandom = game.add.button(buttonElectric.position.x + 40, 53, 'buttonRandom', this.actionOnClick, this, 0, 0, 0);
    buttonTurner = game.add.button(buttonRandom.position.x + 40, 53, 'buttonTurner', this.actionOnClick, this, 0, 0, 0);

    buttonElectric.onInputOver.add(this.over, this);
    buttonElectric.onInputOut.add(this.out, this);
    buttonRandom.onInputOver.add(this.over, this);
    buttonRandom.onInputOut.add(this.out, this);
    buttonTurner.onInputOver.add(this.over, this);
    buttonTurner.onInputOut.add(this.out, this);

}


manaTextTimer = game.time.create(false);
turnTextTimer = game.time.create(false);

//alert("I'm with " + Characters[0].getName() + " and " + Characters[1].getName());



var style = { font: "30px Almendra", fill: "#999999", align: "center" };
textCenter = game.add.text(game.world.centerX, game.world.centerY, "", style);
textCenter.anchor.set(0.5);
textCenter.stroke = '#000000';
textCenter.strokeThickness = 6;


var style2 = { font: "20px Almendra", fill: "#43d637", align: "center" };
text2 = game.add.text(game.world.centerX , game.world.centerY - 95, "", style2);
text2.anchor.set(0.5);
text2.stroke = '#000000';
text2.strokeThickness = 6;
        
var style3 = { font: "15px Almendra", fill: "#7070db", align: "center" };
text3 = game.add.text(game.world.centerX, game.world.centerY - 75, "", style3);
text3.anchor.set(0.5);
text3.stroke = '#000000';
text3.strokeThickness = 6;

var style4 = { font: "15px Almendra", fill: "#cc6600", align: "center" };
text4 = game.add.text(game.world.centerX, game.world.centerY - 55, "", style4);
text4.anchor.set(0.5);
text4.stroke = '#000000';
text4.strokeThickness = 6;


textPotionCheck = game.add.text(game.world.centerX, game.world.centerY - 75, "", style3);
textPotionCheck.anchor.set(0.5);


replenish = game.add.audio('replenish');
healSound = game.add.audio('heal');
manaSound = game.add.audio('mana');

drainSound = game.add.audio('drain');
poisonSound = game.add.audio('poison');
pierceSound = game.add.audio('pierce');

darkspellSound = game.add.audio('darkspell');
staffspellSound = game.add.audio('staffspell');
decreaseSound = game.add.audio('decrease');

heavysoulSound = game.add.audio('heavysoul');
burnSound = game.add.audio('burn');
evasionSound = game.add.audio('evasion');

electricSound = game.add.audio('electric');
randomSound = game.add.audio('random');
turnerSound = game.add.audio('turner');





game.sound.setDecodedCallback([mainSound4,mainSound3,mainSound2,electricSound,randomSound,turnerSound,heavysoulSound,burnSound,evasionSound, drainSound, poisonSound, pierceSound, mainSound, youWinSound, replenish, healSound, manaSound, replenish, darkspellSound, staffspellSound, decreaseSound], this.start, this);

buttonHPotion = game.add.button(game.world.centerX - 290, 90, 'buttonHPotion', this.actionOnClickPotion, this, 0, 0, 0);
buttonMPotion = game.add.button(buttonHPotion.position.x + 40, buttonHPotion.position.y, 'buttonMPotion', this.actionOnClickPotion, this, 0, 0, 0);
buttonTPotion = game.add.button(buttonMPotion.position.x + 40, buttonHPotion.position.y, 'buttonTPotion', this.actionOnClickPotion, this, 0, 0, 0);


buttonHPotion.onInputOver.add(this.over, this);
buttonHPotion.onInputOut.add(this.out, this);
buttonMPotion.onInputOver.add(this.over, this);
buttonMPotion.onInputOut.add(this.out, this);
buttonTPotion.onInputOver.add(this.over, this);
buttonTPotion.onInputOut.add(this.out, this);


    },
    update: function () {

        this.checkPotion();
        this.checkHP();
        this.checkMana();
        this.checkTurnPt();
    },

    actionOnClickPotion: function(button) {
    switch (button) {
        case buttonHPotion:
            if (turn % 2 == mainState.charTurnW1 && mainState.selectionboolW1==true && hp<100 && mana >0) {
                healSound.play();
                myHealthBar.setPercent(hp = hp + 20);
                myManaBar.setPercent(mana = mana - 30);
                turn++;

            }
            else if (turn % 2 == mainState.charTurnW2 && mainState.selectionboolW2 == true && hp2 < 100 && mana2 > 0) {
                healSound.play();
                myHealthBar2.setPercent(hp2 = hp2 + 20);
                myManaBar2.setPercent(mana2 = mana2 - 30);
                turn++;
            }
            else if (turn % 2 == mainState.charTurnW3 && mainState.selectionboolW3 == true && hp3 < 100 && mana3 > 0) {
                healSound.play();
                myHealthBar3.setPercent(hp3 = hp3 + 20);
                myManaBar3.setPercent(mana3 = mana3 - 30);
                turn++;
            }
            else if (turn % 2 == mainState.charTurnW4 && mainState.selectionboolW4 == true && hp4 < 100 && mana4 > 0) {
                healSound.play();
                myHealthBar4.setPercent(hp4 = hp4 + 20);
                myManaBar4.setPercent(mana4 = mana4 - 30);
                turn++;
            } else if (mana <= 0 || mana2 <= 0 || mana3 <= 0 || mana4 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();


            }
break;
        case buttonMPotion:
            if (turn % 2 == mainState.charTurnW1 && mainState.selectionboolW1 == true && mana<100) {
                manaSound.play();
        myManaBar.setPercent(mana = mana + 10);
        turn++;
    }
            else if (turn % 2 == mainState.charTurnW2 && mainState.selectionboolW2 == true && mana2 < 100) {
                manaSound.play();
        myManaBar2.setPercent(mana2 = mana2 + 10);
        turn++;
            } else if (turn % 2 == mainState.charTurnW3 && mainState.selectionboolW3 == true && mana3 < 100) {
                manaSound.play();
                myManaBar3.setPercent(mana3 = mana3 + 10);
                turn++;
            } else if (turn % 2 == mainState.charTurnW4 && mainState.selectionboolW4 == true && mana4 < 100) {
                manaSound.play();
                myManaBar4.setPercent(mana4 = mana4 + 10);
                turn++;
            }
break;
        case buttonTPotion:
            if (turn % 2 == mainState.charTurnW1 && turnPotionCountW1 > 0 && mainState.selectionboolW1 == true) {
                replenish.play();
                 buttonPierceTurn = 2;
                 buttonDrainTurn = 1;
                 buttonPoisonTurn = 1;
                 turnPotionCountW1 --;
                 turn++;
            }
            else if (turn % 2 == mainState.charTurnW2 && turnPotionCountW2 > 0 && mainState.selectionboolW2 == true) {
                 replenish.play();
                 buttonDecreaseTurn = 1;
                 buttonStaffTurn = 2;
                 buttonDarkspellTurn = 1;
                 turnPotionCountW2 --;
                 turn++;
            }
            else if (turn % 2 == mainState.charTurnW3 && turnPotionCountW3 > 0 && mainState.selectionboolW3 == true) {
                replenish.play();
                buttonHeavysoulTurn = 2;
                buttonEvasionTurn = 1;
                buttonBurnTurn = 1;
                turnPotionCountW3--;
                turn++;
            } else if (turn % 2 == mainState.charTurnW4 && turnPotionCountW4 > 0 && mainState.selectionboolW4 == true) {
                replenish.play();
                buttonElectricTurn = 2;
                buttonRandomTurn = 2;
                buttonTurnerTurn = 1;
                turnPotionCountW4--;
                turn++;
            } else if (turnPotionCountW4 == 0 || turnPotionCountW3 == 0 || turnPotionCountW2 == 0 || turnPotionCountW1 == 0) {

                textCenter.text = "No Turn";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();

            }
            break;
        default:
break;
    }
    if (turn == 12) {
        replenish.play();
        textCenter.text = "Turn Potions Replenished";
        manaTextTimer.loop(3000, this.manaCheck, this);
        manaTextTimer.start();
    }
    },
    up: function () {

      
},
    over: function(button) {
    
    
switch(button){
    case buttonPierce:
        text2.text = "Pierce";
        text3.text = "Mana Cost : "  + manaPierce;
        text4.text = buttonPierceTurn + " turn";
        break;
    case buttonDrain:
        text2.text = "Health Drain";
        text3.text = "Mana Cost : "  + manaDrain;
        text4.text = buttonDrainTurn + " turn";
        break;
    case buttonPoison :
        text2.text = "Posion";
        text3.text = "Mana Cost : " + manaPoison;
        text4.text = buttonPoisonTurn + " turn";
        break;
    case buttonDarkspell:
        text2.text = "Dark Spell";
        text3.text = "Mana Cost : " + manaDarkSpell;
        text4.text = buttonDarkspellTurn + " turn";
        break;
    case buttonDecrease:
        text2.text = "Decrease Damage";
        text3.text = "Mana Cost : " + manaDecrease;
        text4.text = buttonDecreaseTurn + " turn";
        break;
    case buttonStaff:
        text2.text = "Staff Spell";
        text3.text = "Mana Cost : " + manaStaff;
        text4.text = buttonStaffTurn + " turn";
        break;
    case buttonHeavysoul:
        text2.text = "Heavy Soul";
        text3.text = "Mana Cost : " + manaHeavysoul;
        text4.text = buttonHeavysoulTurn + " turn";
        break;
    case buttonEvasion:
        text2.text = "Evasion";
        text3.text = "Mana Cost : " + manaEvasion;
        text4.text = buttonEvasionTurn + " turn";
        break;
    case buttonBurn:
        text2.text = "Burn";
        text3.text = "Mana Cost : " + manaBurn;
        text4.text = buttonBurnTurn + " turn";
        break;
    case buttonElectric:
        text2.text = "Electric";
        text3.text = "Mana Cost : " + manaElectric;
        text4.text = buttonElectricTurn + " turn";
        break;
    case buttonRandom:
        text2.text = "Random Damage";
        text3.text = "Mana Cost : " + manaRandom;
        text4.text = buttonRandomTurn + " turn";
        break;
    case buttonTurner:
        text2.text = "Turner";
        text3.text = "Mana Cost : " + manaTurner;
        text4.text = buttonTurnerTurn + " turn";
        break;
    case buttonHPotion:
        text2.text = "Healt Potion";
        text3.text = "Heal for +20";
        break;
    case buttonMPotion:
        text2.text = "Mana Potion";
        text3.text = "Get Mana for +10";
        break;
    case buttonTPotion:
        text2.text = "Turn Potion";
        text3.text = "Use it to replenish your skills"
        if (turn % 2 == mainState.charTurnW1 &&  mainState.selectionboolW1 == true) {
            text4.text = turnPotionCountW1 + " Turn";
        } else if (turn % 2 == mainState.charTurnW2 && mainState.selectionboolW2 == true) {
            text4.text = turnPotionCountW2 + " Turn";
        } else if (turn % 2 == mainState.charTurnW3 && mainState.selectionboolW3 == true) {
            text4.text = turnPotionCountW3 + " Turn";
        } else if (turn % 2 == mainState.charTurnW4 && mainState.selectionboolW4 == true) {
            text4.text = turnPotionCountW4 + " Turn";
        }

        break;
    default:
        break;
}
    },
    out: function() {
    
text2.text = "";
text3.text = "";
text4.text = "";
    },
    actionOnClick: function (button) {
        if (turn == 12) {
            replenish.play();
            textCenter.text = "Turn Potions Replenished";
            turnTextTimer.loop(3000, this.manaCheck, this);
            turnTextTimer.start();
        }

    switch(button){
        case buttonPierce:
            if (turn % 2 == mainState.charTurnW1 && buttonPierceTurn > 0 && mana > 0) {
                pierceSound.play();
                Characters[0].Pierce();
                myManaBar.setPercent(mana = mana - manaPierce);
                if (mainState.selectionboolW2 == true) {
                    myHealthBar2.setPercent(hp2 = hp2 - damagePierce);
                } else if (mainState.selectionboolW3 == true) {
                    myHealthBar3.setPercent(hp3 = hp3 - damagePierce);
                } else if (mainState.selectionboolW4 == true) {
                    myHealthBar4.setPercent(hp4 = hp4 - damagePierce);
                }
                turn++;
                buttonPierceTurn--;

            } else if (mana <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            
        break;
        case buttonDrain:

            if (turn % 2 == mainState.charTurnW1 && buttonDrainTurn > 0 && mana > 0) {
        drainSound.play();
        Characters[0].VitaltyDrain();
        myManaBar.setPercent(mana = mana - manaDrain);
        if (mainState.selectionboolW2 == true) {
            myHealthBar2.setPercent(hp2 = hp2 - damageDrain);
        } else if (mainState.selectionboolW3 == true) {
            myHealthBar3.setPercent(hp3 = hp3 - damageDrain);
        } else if (mainState.selectionboolW4 == true) {
            myHealthBar4.setPercent(hp4 = hp4 - damageDrain);
        }
        
        myHealthBar.setPercent(hp = hp + damageDrain);
        turn++;
        buttonDrainTurn--;
            } else if (mana <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }

            
break;
        case buttonPoison:

            if (turn % 2 == mainState.charTurnW1 && buttonPoisonTurn > 0 && mana > 0) {
        poisonSound.play();
        Characters[0].Poison();
        myManaBar.setPercent(mana = mana - manaPoison);
        poisonBool = true;
        turn++;
        buttonPoisonTurn--;
            } else if (mana <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }

               
break;
        case buttonDarkspell:
            if (turn % 2 == mainState.charTurnW2 && buttonDarkspellTurn > 0 && mana2 > 0) {
                darkspellSound.play();
                console.log(mainState.charTurnW2);
                Characters[1].Darkspell();
                myManaBar2.setPercent(mana2 = mana2 - manaDarkSpell);

        if (mainState.selectionboolW1 == true) {
            myHealthBar.setPercent(hp = hp - damageDarkSpell);
        } else if (mainState.selectionboolW3 == true) {
            myHealthBar3.setPercent(hp3 = hp3 - damageDarkSpell);
        } else if (mainState.selectionboolW4 == true) {
            myHealthBar4.setPercent(hp4 = hp4 - damageDarkSpell);
        }
        damageDarkSpell = damageDarkSpell + 10;
        turn++;
        buttonDarkspellTurn--;
        this.poisonStart();
            } else if (mana2 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
    
break;
        case buttonDecrease:
            if (turn % 2 == mainState.charTurnW2 && buttonDecreaseTurn > 0 && mana2 > 0) {
                decreaseSound.play();
                Characters[1].Decrease();
                myManaBar2.setPercent(mana2 = mana2 - manaDecrease);
        damagePierce = damagePierce - 10;
        damageDrain = damageDrain - 10;
        damageHeavysoul = damageHeavysoul - 10;
        if (W2bool == true) {
            damageBurn = damageBurn - 10;
            damageElectric = damageElectric - 10;
        }
        this.damageRandom = this.damageRandom - 10;
        turn++;
        buttonDecreaseTurn--;
        this.poisonStart();
            } else if (mana2 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            
break;
        case buttonStaff:
            if (turn % 2 == mainState.charTurnW2 && buttonStaffTurn > 0 && mana2 > 0) {
                staffspellSound.play();
                Characters[1].Staff();
                myManaBar2.setPercent(mana2 = mana2 - manaStaff);
        if (mainState.selectionboolW1 == true) {
            myHealthBar.setPercent(hp = hp - damageStaff);
        } else if (mainState.selectionboolW3 == true) {
            myHealthBar3.setPercent(hp3 = hp3 - damageStaff);
        } else if (mainState.selectionboolW4 == true) {
            myHealthBar4.setPercent(hp4 = hp4 - damageStaff);
        }
        turn++;
        buttonStaffTurn--;
        this.poisonStart();
            } else if (mana2 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            break;
        case buttonHeavysoul:

            

            if (turn % 2 == mainState.charTurnW3 && buttonHeavysoulTurn > 0 && mana3 > 0) {
                myManaBar3.setPercent(mana3 = mana3 - manaHeavysoul);
                Characters[2].Heavysoul();
                if (mainState.selectionboolW1 == true) {
                    myHealthBar.setPercent(hp = hp - damageHeavysoul);
                } else if (mainState.selectionboolW2 == true) {
                    myHealthBar2.setPercent(hp2 = hp2 - damageHeavysoul);
                } else if (mainState.selectionboolW4 == true) {
                    myHealthBar4.setPercent(hp4 = hp4 - damageHeavysoul);
                }
                heavysoulSound.play();
                turn++;
                buttonHeavysoulTurn--;
                this.poisonStart();
            } else if (mana3 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            break;
        case buttonBurn:
            
            if (turn % 2 == mainState.charTurnW3 && buttonBurnTurn > 0 && mana3 > 0) {
                myManaBar3.setPercent(mana3 = mana3 - manaBurn);
                Characters[2].Burn();
                if (mainState.selectionboolW1 == true) {
                    myHealthBar.setPercent(hp = hp - damageBurn);
                    myManaBar.setPercent(mana = mana - damageBurn);
                } else if (mainState.selectionboolW2 == true) {
                    myHealthBar2.setPercent(hp2 = hp2 - damageBurn);
                    myManaBar2.setPercent(mana2 = mana2 - damageBurn);
                } else if (mainState.selectionboolW4 == true) {
                    myHealthBar4.setPercent(hp4 = hp4 - damageBurn);
                    myManaBar4.setPercent(mana4 = mana4 - damageBurn);
                }
                
                burnSound.play();
                turn++;
                buttonBurnTurn--;
                this.poisonStart();
            } else if (mana3 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            break;
        case buttonEvasion:

            if (turn % 2 == mainState.charTurnW3 && buttonEvasionTurn > 0 && mana3 > 0) {

                myManaBar3.setPercent(mana3 = mana3 - manaEvasion);
                Characters[2].Evasion();
                
                if (mainState.selectionboolW1 == true) {
                    manaPierce = manaPierce + 5;
                    poisonBool = false;

                } else if (mainState.selectionboolW2 == true) {
                    manaStaff = manaStaff + 5;
                    W2bool = false;

                } else if (mainState.selectionboolW3 == true) {
                    manaRandom = manaRandom + 5;
                    W3bool = false;

                }
               
                evasionSound.play();
                turn++;
                buttonEvasionTurn--;
                this.poisonStart();
            } else if (mana3 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            break;
        case buttonElectric:

            if (turn % 2 == mainState.charTurnW4 && buttonElectricTurn > 0 && mana4 > 0) {
                 
                myManaBar4.setPercent(mana4 = mana4 - manaElectric);
                Characters[3].Electric();
                if (mainState.selectionboolW1 == true) {
                    myHealthBar.setPercent(hp = hp - damageElectric);

                } else if (mainState.selectionboolW2 == true) {
                    myHealthBar2.setPercent(hp2 = hp2 - damageElectric);

                } else if (mainState.selectionboolW3 == true) {
                    myHealthBar3.setPercent(hp3 = hp3 - damageElectric);
                }
               
                turn++;
                electricSound.play();
                buttonElectricTurn--;
                this.poisonStart();
            } else if (mana4 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            break;
        case buttonRandom:
            if (turn % 2 == mainState.charTurnW4 && buttonRandomTurn > 0 && mana4 > 0) {

                myManaBar4.setPercent(mana4 = mana4 - manaRandom);
                Characters[3].Random();

                this.damageRandom = game.rnd.integerInRange(20, 40);

                if (mainState.selectionboolW1 == true) {
                    myHealthBar.setPercent(hp = hp - this.damageRandom);

                } else if (mainState.selectionboolW2 == true) {
                    myHealthBar2.setPercent(hp2 = hp2 - this.damageRandom);

                } else if (mainState.selectionboolW3 == true) {
                    myHealthBar3.setPercent(hp3 = hp3 - this.damageRandom);
                }

        
                turn++;
                randomSound.play();
                buttonRandomTurn--;
                this.poisonStart();
            } else if (mana4 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
            break;
        case buttonTurner:
            if (turn % 2 == mainState.charTurnW4 && buttonTurnerTurn > 0 && mana4 > 0) {

                myManaBar4.setPercent(mana4 = mana4 - manaTurner);
                Characters[3].Turner();
                
                if (mainState.selectionboolW1 == true) {
                    buttonPierceTurn = buttonPierceTurn - 1;
                    buttonDrainTurn = buttonDrainTurn - 1;
                    buttonPoisonTurn = buttonPoisonTurn - 1;
                    

                } else if (mainState.selectionboolW2 == true) {
                    buttonDecreaseTurn = buttonDecreaseTurn-1;
                    buttonStaffTurn = buttonStaffTurn-1;
                    buttonDarkspellTurn =buttonDarkspellTurn- 1;
                    
                } else if (mainState.selectionboolW3 == true && W3bool==true) {
                    buttonHeavysoulTurn = buttonHeavysoulTurn - 1;
                    buttonBurnTurn = buttonBurnTurn - 1;
                    buttonEvasionTurn = buttonEvasionTurn - 1;
                    
                }
                
                turn++;
                turnerSound.play();
                buttonTurnerTurn--;
                this.poisonStart();
            } else if (mana4 <= 0) {
                textCenter.text = "No Mana";
                manaTextTimer.loop(3000, this.manaCheck, this);
                manaTextTimer.start();
            }
        default:
            break;

}

},
    poisonStart: function() {

    if (poisonBool == true) {
        if (mainState.selectionboolW2 == true) {
            myHealthBar2.setPercent(hp2 = hp2 - damagePoison);
        } else if (mainState.selectionboolW3 == true) {
            myHealthBar3.setPercent(hp3 = hp3 - damagePoison);
        } else if (mainState.selectionboolW4 == true) {
            myHealthBar4.setPercent(hp4 = hp4 - damagePoison);
        }
        poisonTurn--;
    }
if (poisonTurn == 0) {

    poisonBool = false;
    poisonTurn = 3;
}
    
    },
    checkPotion: function() {
        if (previousTurn % 2 != turn % 2) {
            if (turn % 2 == 0) {
                buttonHPotion.position.x = game.world.centerX - 290;
                buttonMPotion.position.x = buttonHPotion.position.x + 40;
                buttonTPotion.position.x = buttonMPotion.position.x + 40;
                previousTurn++;
        }
        else {
                buttonHPotion.position.x = 560;
                buttonMPotion.position.x = buttonHPotion.position.x + 40;
                buttonTPotion.position.x = buttonMPotion.position.x + 40;
                previousTurn++;
        }
    }
    },
    checkHP: function () {

        if (hp > 100) {
            hp = 100;
        }
        if (hp2 > 100) {
            hp2 = 100;
        }
        if (hp3 > 100) {
            hp3 = 100;
        }
        if (hp4 > 100) {
            hp4 = 100;
        }

        var hpTurnW1 = mainState.charTurnW1 + 1;
        var hpTurnW2 = mainState.charTurnW2 + 1;
        var hpTurnW3 = mainState.charTurnW3 + 1;
        var hpTurnW4 = mainState.charTurnW4 + 1;

        if (mainState.selectionboolW1 == true && hp2 <= 0 || hp3 <= 0 || hp4 <= 0) {
            textCenter.text = "PLAYER " + hpTurnW1  + "  WINS"
            buttonReplay = game.add.button(game.world.centerX - 75, game.world.centerY + 20, 'buttonReplay', this.gameOver, this, 0, 0, 0);
        
        } else if (mainState.selectionboolW2 == true &&  hp <= 0 || hp3 <= 0 || hp4 <= 0) {
           
            textCenter.text = "PLAYER " + hpTurnW2 + "  WINS"
            buttonReplay = game.add.button(game.world.centerX - 75, game.world.centerY + 20, 'buttonReplay', this.gameOver, this, 0, 0, 0);

    }
        else if (mainState.selectionboolW3 == true &&  hp <= 0 || hp2 <= 0 || hp4 <= 0 ) {
           
            textCenter.text = "PLAYER " + hpTurnW3 + "  WINS"
            buttonReplay = game.add.button(game.world.centerX - 75, game.world.centerY + 20, 'buttonReplay', this.gameOver, this, 0, 0, 0);

        } else if (hp2 <= 0 || hp3 <= 0 || hp <= 0 && mainState.selectionboolW4 == true) {
           
            textCenter.text = "PLAYER " + hpTurnW4 + "  WINS"
            buttonReplay = game.add.button(game.world.centerX - 75, game.world.centerY + 20, 'buttonReplay', this.gameOver, this, 0, 0, 0);

    }
    },

    checkMana: function(){
        if (mana > 100) {
            mana = 100;
        }
        if (mana2 > 100) {
            mana2 = 100;
        }
        if (mana3 > 100) {
            mana3 = 100;
        }
        if (mana4 > 100) {
            mana4 = 100;
        }

        if (mana < 0) {
            mana = 0;
        }
        if (mana2 < 0) {
            mana2 = 0;
        }
        if (mana3 < 0) {
            mana3 = 0;
        }
        if (mana4 < 0) {
            mana4 = 0;
        }
    },
    manaCheck: function () {

        
        textCenter.text = "";
       
    },
    
    mainStopped: function() {
    main.play();

    },
    checkTurnPt: function(){
        if (turn == 12) {
            turnPotionCountW1 = 1;
            turnPotionCountW2 = 1;
            turnPotionCountW3 = 1;
            turnPotionCountW4 = 1;
        }
    },
    gameOver: function () {

        youWinSound.play();
        this.game.world.removeAll();
        game.state.start("boot");

    },
    start: function() {
       
}
};









































