﻿var buttonWizard1;
var buttonWizard2;
var buttonWizard3;
var buttonWizard4;

var secTurnW1 = false;

var selectyourchar;

var timer;
var mainState = {
    preload: function () {

        game.load.image('gandalf', 'Images/wizard-center.png');

        game.load.atlas('wizard1', 'Sprites/wizard.png', 'Sprites/wizard_.json',
Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
            
        game.load.atlas('wizard2', 'Sprites/wizard2_.png', 'Sprites/wizard2__.json',
        Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
        
        game.load.atlas('wizard3', 'Sprites/wizard3_.png', 'Sprites/wizard3.json',
        Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

        game.load.atlas('wizard4', 'Sprites/wizard4.png', 'Sprites/wizard4.json',
        Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

        game.load.audio('charselect', 'SoundEffects/charselect.mp3');
        game.load.audio('selectyourchar', 'SoundEffects/selectyourchar.mp3');
        game.load.audio('clickover', 'SoundEffects/clickover.mp3');
        game.load.audio('selected', 'SoundEffects/selected.mp3');

        game.load.image('background', 'Pictures/charac_select.png');
        game.load.spritesheet('buttonWizard1', 'Images/_wizard_one.png', 63, 81);
        game.load.spritesheet('buttonWizard2', 'Images/_wizard_two.png', 63, 81);

        game.load.spritesheet('buttonWizard3', 'Images/_wizard_three.png', 63, 81);
        game.load.spritesheet('buttonWizard4', 'Images/wizard_four.png', 63, 81);

        game.load.image('wizard1A', 'Images/wizard1A.png');
        game.load.image('wizard2A', 'Images/wizard2A.png');
        
    },
    clickover:0,
    charselect: 0,
    selected:0,
    selectionboolW1: false,
    selectionboolW2: false,
    selectionboolW3: false,
    selectionboolW4:false,
    xW1Poz: 0,
    xW2Poz: 0,
    xW3Poz: 0,
    xW4Poz: 0,
    yW1Poz: 0,
    yW2Poz: 0,
    yW3Poz: 0,
    yW4Poz: 0,
    scaleW1: 0,
    scaleW2: 0,
    scaleW3: 0,
    scaleW4:0,
    selectTurn: 0,
    barHW1: 0,
    barMW1: 0,
    barHW2: 0,
    barMW2: 0,
    barHW3: 0,
    barMW3: 0,
    barHW4: 0,
    barMW4: 0,
    buttonPozW1: 0,
    buttonPozW2: 0,
    buttonPozW3: 0,
    buttonPozW4:0,
    charTurnW1: 0,
    charTurnW2: 0,
    charTurnW3: 0,
    charTurnW4: 0,

    create: function () {
        var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'background');

        Background.anchor.setTo(0.5, 0.5);
        
        var Gandalf = game.add.sprite(game.world.centerX - 3, game.world.centerY - 35, 'gandalf');
        Gandalf.anchor.setTo(0.5, 0.5);

        this.charselect = game.add.audio('charselect');
        selectyourchar = game.add.audio('selectyourchar');
        this.clickover = game.add.audio('clickover');
        this.selected = game.add.audio('selected');

        this.charselect.play();
      
        selectyourchar.play();
       
        timer = game.time.create(false);
        
        
        buttonWizard1 = game.add.button(game.world.centerX - 101, 41, 'buttonWizard1', this.actionOnClick, this, 0, 1, 2);
        buttonWizard2 = game.add.button(buttonWizard1.position.x + 135, 41, 'buttonWizard2', this.actionOnClick, this, 0, 1, 2);
        buttonWizard3 = game.add.button(buttonWizard2.position.x  , buttonWizard2.position.y + 82, 'buttonWizard3', this.actionOnClick, this, 0, 1, 2);
        buttonWizard4 = game.add.button(buttonWizard3.position.x - 135, buttonWizard3.position.y, 'buttonWizard4', this.actionOnClick, this, 0, 1, 2);

        buttonWizard1.onInputOver.add(this.over, this);
        buttonWizard1.onInputOut.add(this.out, this);
        buttonWizard2.onInputOver.add(this.over, this);
        buttonWizard2.onInputOut.add(this.out, this);
        buttonWizard3.onInputOver.add(this.over, this);
        buttonWizard3.onInputOut.add(this.out, this);
        buttonWizard4.onInputOver.add(this.over, this);
        buttonWizard4.onInputOut.add(this.out, this);
    },
    actionOnClick: function (button) {

        this.selectTurn++;
        this.selected.play();
        switch (button) {
            case buttonWizard1:
                this.selectionboolW1 = true;
                if (this.selectTurn == 1) {
                    this.xW1Poz = 280;
                    this.yW1Poz = 110;
                    this.scaleW1 = -1;
                    this.barHW1 = 200;
                    this.barMW1 = 175;
                    this.buttonPozW1 = 180;
                    this.charTurnW1 = 0;

                    var player1 = new Character("Wizard1");
                    player1.init();
                    Characters[0] = player1;
                    Characters[0].VitaltyDrain();
                    this.xW1Poz = 350;
                    this.yW1Poz = 225;

                    secTurnW1=true;

                    

                } else if (this.selectTurn == 2 && secTurnW1!=true) {
                    this.xW1Poz = 500;
                    this.yW1Poz = 110;
                    this.scaleW1 = 1;
                    this.barHW1 = 580;
                    this.barMW1 = 605;
                    this.buttonPozW1 = 640;
                    this.charTurnW1 = 1;
                   
                    var player1 = new Character("Wizard1");
                    player1.init();
                    Characters[0] = player1;
                    Characters[0].VitaltyDrain();

                    this.xW1Poz = 450;
                    this.yW1Poz = 225;

                    
                    
                }
                break;
            case buttonWizard2:
                this.selectionboolW2 = true;
                if (this.selectTurn == 1) {
                    this.xW2Poz = 270;
                    this.yW2Poz = 75;
                    this.scaleW2 = -1;
                    this.barHW2 = 200;
                    this.barMW2 = 175;
                    this.buttonPozW2 = 100;
                    this.charTurnW2 = 0;
                    var player2 = new Character("Wizard2");
                    player2.init2();
                    Characters[1] = player2;
                    Characters[1].Darkspell();

                    this.xW2Poz = 300;
                    this.yW2Poz = 180;

                } else if (this.selectTurn == 2) {
                    this.xW2Poz = 510;
                    this.yW2Poz = 75;
                    this.scaleW2 = 1;
                    this.barHW2 = 580;
                    this.barMW2 = 605;
                    this.buttonPozW2 = 560;
                    this.charTurnW2 = 1;

                    var player2 = new Character("Wizard2");
                    player2.init2();
                    Characters[1] = player2;
                    Characters[1].Darkspell();

                    this.xW2Poz = 450;
                    this.yW2Poz = 180;

                }
                break;
            case buttonWizard3:
                this.selectionboolW3 = true;
                if (this.selectTurn == 1) {
                    this.xW3Poz = 270;
                    this.yW3Poz = 75;
                    this.scaleW3 = -1;
                    this.barHW3 = 200;
                    this.barMW3 = 175;
                    this.buttonPozW3 = 100;
                    this.charTurnW3 = 0;

                    var player3 = new Character("Wizard3");
                    player3.init3();
                    Characters[2] = player3;
                    Characters[2].Burn();
                    
                    this.xW3Poz = 320;
                    this.yW3Poz = 180;

                } else if (this.selectTurn == 2) {
                    this.xW3Poz = 500;
                    this.yW3Poz = 75;
                    this.scaleW3 = 1;
                    this.barHW3 = 580;
                    this.barMW3 = 605;
                    this.buttonPozW3 = 560;
                    this.charTurnW3 = 1;

                    var player3 = new Character("Wizard3");
                    player3.init3();
                    Characters[2] = player3;
                    Characters[2].Burn();

                    this.xW3Poz = 450;
                    this.yW3Poz = 180;

                }
                break;
            case buttonWizard4:
                this.selectionboolW4 = true;
                if (this.selectTurn == 1) {
                    this.xW4Poz = 270;
                    this.yW4Poz = 75;
                    this.scaleW4 = -1;
                    this.barHW4 = 200;
                    this.barMW4 = 175;
                    this.buttonPozW4 = 100;
                    this.charTurnW4 = 0;

                    var player3 = new Character("Wizard3");
                    player3.init4();
                    Characters[2] = player3;
                    Characters[2].Random();


                    this.xW4Poz = 320;
                    this.yW4Poz = 180;

                } else if (this.selectTurn == 2) {
                    this.xW4Poz = 500;
                    this.yW4Poz = 75;
                    this.scaleW4 = 1;
                    this.barHW4 = 580;
                    this.barMW4 = 605;
                    this.buttonPozW4 = 560;
                    this.charTurnW4 = 1;

                    var player3 = new Character("Wizard3");
                    player3.init4();
                    Characters[2] = player3;
                    Characters[2].Random();

                    this.xW4Poz = 450;
                    this.yW4Poz = 180;
                }
            default:
                break;

                
        }

        if (this.selectTurn == 2 ) {
            timer.loop(3000, this.start, this);
            timer.start();
            
        }
    },
    over: function (button) {

        this.clickover.play();

      
            
            
        
        

    },
    out: function () {
        
    },
    start: function () {

        game.state.start('backgroundmenu');

    }

};