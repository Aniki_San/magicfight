﻿
var Character = function (name) {
    this.name = name;
};
Character.prototype.getName = function () {
    return this.name;
};
Character.prototype.getSprite = function () {
    return this.sprite;
};
Character.prototype.getIsLoop = function () {
    return this.isLoop;
};

Character.prototype.init = function () {
    this.isLoop = false;
    this.sprite = game.add.sprite(mainState.xW1Poz, mainState.yW1Poz, 'wizard1');
    this.sprite.animations.add('stand', [24]);
    this.sprite.animations.add('pierce', [0, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 24]);
    this.sprite.animations.add('poison', [11, 16, 17, 18, 19, 20, 21, 22, 23, 12, 13, 14, 15, 24]);
    this.sprite.animations.add('vitaltydrain', [25, 28, 29, 30, 31, 32, 33, 34, 35, 26, 27, 24]);
    this.sprite.animations.play('stand', 8, true);
    this.sprite.anchor.setTo(0, 1);
    this.sprite.scale.x *= mainState.scaleW1;
}

Character.prototype.Pierce = function () {
    if (this.sprite.animations.frame == 24) {
        this.sprite.animations.play('pierce', 5, false);
        console.log("pierce");
    }

};


Character.prototype.VitaltyDrain = function () {

    if (this.sprite.animations.frame == 24) {
        this.sprite.animations.play('vitaltydrain', 5, false);
        console.log("VitaltyDrain");
    }

};

Character.prototype.Poison = function () {

    if (this.sprite.animations.frame == 24) {
        this.sprite.animations.play('poison', 5, false);
        console.log("poison");
    }

};



Character.prototype.init2 = function () {
    this.isLoop = false;
    this.sprite = game.add.sprite(mainState.xW2Poz, mainState.yW2Poz, 'wizard2');
    this.sprite.animations.add('stand', [15]);
    this.sprite.animations.add('darkspell', [0, 1, 2, 3, 15]);
    this.sprite.animations.add('decrease', [4,5,6,7,8,15]);
    this.sprite.animations.add('staff', [9,10,11,12,13,15]);
    this.sprite.animations.play('stand', 8, true);
    this.sprite.anchor.setTo(0, 0);
    this.sprite.scale.x *= mainState.scaleW2;
}

Character.prototype.Darkspell = function () {

    if (this.sprite.animations.frame == 15) {
        this.sprite.animations.play('darkspell', 5, false);
        console.log("darkspell");
    }

};

Character.prototype.Decrease = function () {

    if (this.sprite.animations.frame == 15) {
        this.sprite.animations.play('decrease', 5, false);
        console.log("decrease");
    }

};

Character.prototype.Staff = function () {

    if (this.sprite.animations.frame == 15) {
        this.sprite.animations.play('staff', 5, false);
        console.log("staff");
    }

};

Character.prototype.init3 = function () {
    this.isLoop = false;
    this.sprite = game.add.sprite(mainState.xW3Poz, mainState.yW3Poz, 'wizard3');
    this.sprite.animations.add('stand', [41]);
    this.sprite.animations.add('heavysoul', [31, 39, 40, 41, 42, 43, 44, 45, 46, 32, 33, 34, 35, 36, 37, 38, 41]);
    this.sprite.animations.add('evasion', [17, 23, 24, 25, 26, 27, 28, 29, 18, 19, 20, 21, 22, 41]);
    this.sprite.animations.add('burn', [0, 9, 10, 11, 12, 13, 14, 15, 16, 1, 2, 3, 4, 5, 6, 7, 8, 41]);
    this.sprite.animations.play('stand', 8, true);
    this.sprite.anchor.setTo(0, 0);
    this.sprite.scale.x *= mainState.scaleW3;
}

Character.prototype.Heavysoul = function () {

    if (this.sprite.animations.frame == 41) {
        this.sprite.animations.play('heavysoul', 7, false);
        console.log("heavysoul");
    }

};

Character.prototype.Evasion = function () {

    if (this.sprite.animations.frame == 41) {
        this.sprite.animations.play('evasion', 3, false);
        console.log("evasion");
    }

};

Character.prototype.Burn = function () {

    if (this.sprite.animations.frame == 41) {
        this.sprite.animations.play('burn', 8, false);
        console.log("burn");
    }

};


Character.prototype.init4 = function () {
    this.isLoop = false;
    this.sprite = game.add.sprite(mainState.xW4Poz, mainState.yW4Poz, 'wizard4');
    this.sprite.animations.add('stand', [0]);
    this.sprite.animations.add('electric', [0, 8, 9, 10, 11, 12, 13, 14, 15, 2, 3, 4, 5, 6, 7, 0]);
    this.sprite.animations.add('random', [16, 24, 25, 26, 27, 28, 29, 17, 18, 19, 20, 21, 22, 23, 0]);
    this.sprite.animations.add('turner', [30, 31, 32, 33, 34, 35, 36, 0]);
    this.sprite.animations.play('stand', 8, true);
    this.sprite.anchor.setTo(0, 0);
    this.sprite.scale.x *= mainState.scaleW4;
}


Character.prototype.Electric = function () {

    if (this.sprite.animations.frame == 0) {
        this.sprite.animations.play('electric', 8, false);
        console.log("electric");
    }

};

Character.prototype.Random = function () {

    if (this.sprite.animations.frame == 0) {
        this.sprite.animations.play('random', 8, false);
        console.log("random");
    }

};

Character.prototype.Turner = function () {

    if (this.sprite.animations.frame == 0) {
        this.sprite.animations.play('turner', 8, false);
        console.log("turner");
    }

};



//var GameObjects = window.GameObjects || {};
//GameObjects.Character = (function () {
//    return {
//        DarkSpell: function () {
//            if (sprite.animations.frame == 24){
//                sprite.animations.play('darkspell', 5, false);
//                console.log("darkspell");
//            }
//        },
//        Stand: function () {
//            sprite.animations.play('stand', 5, true);
//            console.log("stand");
//        },
//        Poison: function () {
//            if (sprite.animations.frame == 24) {
//                sprite.animations.play('poison', 5, false);
//                console.log("poison");
//            }
//            sprite.animations.isFinished = true;
//        },
//        Vitaltydrain: function () {
//            if (sprite.animations.frame == 24) {
//                sprite.animations.play('vitaltydrain', 5, false);
//                console.log("vitaltydrain");
//            }
            
//        }
//    };
        

//});


